<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">

    <title>My page title</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300%7CSonsie+One" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/style.css">
  </head>

  <body>

    <header>
        <div class="header-content">
            <img src="img/icono-principal.png" alt="Icono de Configuración" width="90" height="90">
            <h1>A través de mis ojos</h1>
        </div>
    </header>


    <nav>
      <ul>
        <li><a href="#">Inicio</a></li>
        <li><a href="#">Quienes somos</a></li>
        <li><a href="#"><img src="img/icono-tuerca.png" alt="Icono de Configuración" width="50" height="50" style="
        width: 16px;
        height: 16px;
        margin-top: 15p;
        "> Configuración</a></li>
        <li><a href="#"><img src="img/icono-logout.png" alt="Icono de Salir" width="50" height="50" style="
        width: 16px;
        height: 16px;
        margin-top: 15p;
        "> Salir</a></li>
        

      </ul>

    </nav>

    <main>

      <article>
        <h2>Objetivo</h2>

        <p>El proyecto consiste en desarrollar un sitio web que aborde la temática de la discapacidad
visual, proporcionando información valiosa tanto para personas con discapacidad visual
como para aquellas sin esta condición. El sitio se centra en la interacción entre los
usuarios, permitiéndoles hacer preguntas, compartir respuestas y votar sobre contenidos
relacionados con la ceguera.</p>

        <h2>Guia del sistema</h2>

        <h3>1. Preguntas y respuestas</h3>
            <p>
                En esta sección, puedes hacer preguntas sobre la discapacidad visual y obtener respuestas de la comunidad.
            </p>

        <h3>2. Artículos interesantes</h3>
            <p>
                Encuentra artículos interesantes relacionados con la discapacidad visual y otros temas relevantes.
            </p>

        <h3>3. Ideas innovadoras</h3>
            <p>
                Explora ideas innovadoras y soluciones relacionadas con la discapacidad visual en esta sección.
            </p>

        <h3>4. Reportes</h3>
            <p>
                Accede a informes y reportes sobre la discapacidad visual, usuarios, logros, etc .
            </p>

        <h3>5. Configuración de usuario</h3>
            <p>
                Personalizar tu perfil y configurar tus preferencias de usuario.
            </p>
      </article>

      <aside>
        <h2>Secciones</h2>

        <ul>
          <li><a href="#">Preguntas y respuestas</a></li>
          <li><a href="#">Articulos interesantes</a></li>
          <li><a href="#">Ideas innovadoras</a></li>
          <li><a href="#">Reportes</a></li>
        </ul>
      </aside>

    </main>

    <footer>
      <p>Aqui pondremos info de redes social y otros enlaces</p>
    </footer>

  </body>
</html>